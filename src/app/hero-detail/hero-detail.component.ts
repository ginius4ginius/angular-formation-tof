import {Component, Input, OnInit} from '@angular/core';
import {Hero} from '../models/hero';
import {ActivatedRoute} from '@angular/router';
import {HeroService} from '../services/hero.service';
import {MessageService} from '../services/message.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  @Input() hero: Hero;

  constructor(private route: ActivatedRoute, private heroService: HeroService, private location: Location,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id).subscribe(
      result => this.hero = result
      , error => {
        this.messageService.add('HeroesComponent : Erreur de récupération des heros');
      }, () => {
        this.messageService.add('HeroesComponent : heroService.getHeroes() completed');
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  /**
   * Méthode de sauvegarde du hero modifié
   */
  save() {
    this.heroService.save(this.hero).subscribe(
      ()=>this.goBack()
    );
  }


}
