import { Component, OnInit } from '@angular/core';
import {Hero} from '../models/hero';
import {HeroService} from '../services/hero.service';
import {MessageService} from '../services/message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[];

  constructor(private heroService: HeroService, private messageService: MessageService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void{
   this.heroService.getHeroes().subscribe(
     result=>this.heroes = result);
  }

  add(name: string): void{
    name = name.trim();
    if(!name){ return; } // si le nom est vide n'éxécute pas la suite.
    this.heroService.add({name} as Hero).subscribe(
      hero=>this.heroes.push(hero));
  }

  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h!== hero); // retrait de la liste des héros
      this.heroService.delete(hero).subscribe(); // requete HTTP delete
  }
}
