import {Injectable} from '@angular/core';
import {Hero} from '../models/hero';
import {HEROES} from '../models/mock.heroes';
import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'api/heroes'; //URL to web api

  httpOptions = {
    headers: new HttpHeaders({'content-type': 'application/json'})
  };

  constructor(private messageService: MessageService, private http: HttpClient) {
  }

  /**
   * GET Heroes from the server.
   */
  getHeroes(): Observable<Hero[]> {
    this.messageService.add('HeroService: Tentative récupération des héros');
    // en commentaire utilisait le mock mock.heroes
    //return of(HEROES);
    return this.http.get<Hero[]>(this.heroesUrl).pipe(
      tap(_ => this.log('HeroService: Héros chargés')),
      catchError(this.handleError<Hero[]>('getHeroes', []))
    );
  }

  /**
   * GET Hero from the server.
   */
  getHero(id: number): Observable<Hero> {
    this.messageService.add(`HeroService: Tentative de récupération du héro avec id: ${id}`);
    // en commentaire utilisait le mock mock.heroes
    //return of(HEROES.find(hero => hero.id === id));
    const heroUrl = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(heroUrl).pipe(
      tap(_ => this.log('HeroService: Héro chargé')),
      catchError(this.handleError<Hero>(`getHero/${id}`))
    );
  }

  /**
   * Méthode de mise à jour des logs.
   * @param message
   */
  private log(message: string) {
    this.messageService.add(message);
  }

  /**
   * Gestionnaire des erreurs.
   * @param operation
   * @param result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.body.error}`);
      return of(result as T);
    };
  }

  /**
   * Méthode de sauvegarde du héro dans le serveur.
   * @param hero
   */
  save(hero: Hero): Observable<any> {
    this.messageService.add(`HeroService: Tentative de modification du héro avec id: ${hero.id}`);
    return this.http.put<Hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap(_ => this.log('HeroService: Héro modifié')),
      catchError(this.handleError<any>(`saveHeroe/${hero.id}`))
    );
  }

  /**
   * Méthode d'ajout d'un hero dans le .
   * @param hero
   */
  add(hero: Hero): Observable<any> {
    this.messageService.add(`HeroService: Tentative d'ajout du héro avec nom: ${hero.name}`);
    return this.http.post<Hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap((newHero: Hero) => this.log(`HeroService: Héro ${newHero.name} ajouté`)),
      catchError(this.handleError<any>(`addHeroe/${hero.id}`))
    );
  }

  /**
   * Méthode de suppression du héro dans le serveur.
   * @param hero
   */
  delete(hero: Hero |number): Observable<any> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const heroUrl = `${this.heroesUrl}/${id}`;
    this.messageService.add(`HeroService: Tentative de suppression du héro avec id: ${id}`);
    return this.http.delete<Hero>(this.heroesUrl,  this.httpOptions).pipe(
      tap((newHero: Hero) => this.log(`HeroService: Héro ${newHero.name} supprimé`)),
      catchError(this.handleError<any>(`addHeroe/${id}`))
    );
  }

  searchHeroes(terme: string): Observable<any>{
    if(!terme.trim()){
      return  of([]);// si terme est un champs vide
    }
    return this.http.get<Hero[]>(`${this.heroesUrl}/?name=${terme}`).pipe(
      tap(x =>x.length ?
      this.log(`Héros trouvés avec recherche : "${terme}"`) :
      this.log(`Héro non trouvé avec recherche : "${terme}"`)),
      catchError(this.handleError<any>('searchHeroes', []))
    );
  }
}
